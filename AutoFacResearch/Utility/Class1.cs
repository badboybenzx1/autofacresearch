﻿using Autofac;
using Bussiness;
using Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Class1
    {
        public static ContainerBuilder RegisterBuilder(ContainerBuilder builder)
        {
            builder.RegisterType<AnnouceBiz>().As<IAnnouce>();

            return builder;
        }
    }
}
