﻿using Autofac;
using Autofac.Core;
using Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace AutoFacResearchApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAnnouce _name;

        public HomeController(IAnnouce name)
        {
            this._name = name;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = _name.Welcome(" benz");

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}